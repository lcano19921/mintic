import csv

#Abrimos el archivo
with open('FundamentosProgramacion\Semana7\Reto\MSFT.csv') as archivo_inicial:
    #Creamos un archivo
    with open('analisis_archivo.csv', 'w', newline = '') as archivo_analisis:
        #leemos el archivo padre
        reader_archivo_inicial = csv.reader(archivo_inicial)
        #iniciamos todo para la escritura sobre el otro archivo '\t' = separado por tabulador )
        writer_archivo_analisis = csv.writer(archivo_analisis, delimiter = '\t')
        #Creamos una lista para almacenar los datos del procesamiento
        data_list = [['Fecha','Mean-Min-Max','Concepto']]

        #Se usa el enumerate para que tome el rango de la cantidad de filas del archivo
        for i, row in enumerate(reader_archivo_inicial):
            #Ignoramos los encabezados
            if i == 0:
                continue
            elif i == 1:
                date_lowest = row[0]
                lowes_value = float(row[3])
                date_highest = row[0]
                highest_value = float(row[2])

            if lowes_value > float(row[3]):
                date_lowest = row[0]
                lowes_value = float(row[3])

            if highest_value < float(row[2]):
                date_highest = row[0]
                highest_value = float(row[2])           

            #Calculamos promedio
            avg = (float(row[2]) + float(row[3]))/2
            #Indica el promedio por fecha
            aux_list = [row[0],avg]

            if avg < 207:
                aux_list.append('MUY BAJO')
            elif avg < 221:
                aux_list.append('BAJO')
            elif avg < 235:
                aux_list.append('MEDIO')
            elif avg < 249:
                aux_list.append('ALTO')
            elif avg >= 249:
                aux_list.append('MUY ALTO')

            data_list.append(aux_list)

print(data_list)


