from tkinter import *

# def parte1():

#     def accion(mensaje):
#         title_label.config(text=mensaje)

#     #Declaramos e inicializamos la ventana principal
#     main_window = Tk() 

#     #Declaramos el marco y sus propiedades
#     main_frame = Frame(main_window, bg='#ff0033', height=500, width=500, cursor='dot')

#     #Lo instanciamos en la interfaz
#     main_frame.pack()

#     #Definimos el label y sus propiedades
#     title_label = Label(main_frame, text="Hello UI World", font=('Arial',11)) 

#     #Lo instanciamos en la interfaz
#     title_label.pack() 

#     boton_text = 'Boton'
#     mensaje = "Este es un parametro"
#     boton = Button(main_frame, text=boton_text, command=lambda: accion(mensaje)) 
#     boton.pack()

#     imagen = PhotoImage(file='FundamentosProgramacion\Semana6\Ejemplos\python-logo.png')
#     body_label1 = Label(main_frame, image=imagen)
#     body_label1.pack()

#     #Usamos esta funcion para permitir que la ventana permanezca abierta durante la ejecución.
#     main_window.mainloop()

def accion():
    text = textbox.get()
    textbox.delete(0, END)
    text_label.config(text=text)

main_window = Tk()
main_frame = Frame(main_window, bg='green', height=500,width=500, padx=50, pady=50)
main_frame.pack()
title_label = Label(main_frame, text="Hello UI World", font=('Arial', 11))
title_label.pack()

boton_text = 'Boton'
boton = Button(main_frame, text=boton_text, command=accion)
boton.pack()

textbox = Entry(main_frame)
textbox.pack()
text_label = Label(main_frame, bg="green")
text_label.pack()
main_window.mainloop()
