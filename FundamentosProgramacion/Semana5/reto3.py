def morseDictionary():
    morse_dictionary = {
        '.-':'A',
        '-...':'B',
        '-.-.':'C',
        '-..':'D',
        '.':'E',
        '..-.':'F',
        '--.':'G',
        '....':'H',
        '..':'I',
        '.---':'J',
        '-.-':'K',
        '.-..':'L',
        '--':'M',
        '-.':'N',
        '---':'O',
        '.--.':'P',
        '--.-':'Q',
        '.-.':'R',
        '...':'S',
        '-':'T',
        '..-':'U',
        '...-':'V',
        '.--':'W',
        '-..-':'X',
        '-.--':'Y',
        '--..':'Z',
        '/':' '
    }
    
    return morse_dictionary
    
    
def morse_traslator(mensaje_a_traducir):
    words_message = mensaje_a_traducir.split()
    morse_dictionary = morseDictionary()

    message_traslate = ''
    for letter in words_message:
        message_traslate += morse_dictionary[letter]
    
    return message_traslate


#PUEDES PROGRAMAR CUANTAS FUNCIONES DESEES 



"""Fin espacio para programar funciones propias"""

def traductor_a_espanol(mensaje_a_traducir):
    #PROGRAMA ACÁ TU SOLUCIÓN
    
    mensaje_traducido = morse_traslator(mensaje_a_traducir)
    
    return mensaje_traducido

print(traductor_a_espanol('- . -. . -- --- ... / -.-. --- -- .. -.. .- / .--. .- .-. .- / - .-. . ... / -.. .. .- ... / -- .- ...'))