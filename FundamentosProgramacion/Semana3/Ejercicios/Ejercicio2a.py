'''Elabore un programa en Python que lea un entero de dos dígitos y produzca como salida los dígitos del número leído con su correspondiente mensaje. Por ejemplo, si el número leído es 27, la salida deberá ser:(sin texto adicional):
2
7'''

#Variable para almacenar el numero ingresado
numero_entero = input('Porfavor ingrese un numero entero de dos digitos: -->')

#Lo trabajamos como string para separarlo de forma más sencilla
numero1 = numero_entero[0:1]
numero2 = numero_entero[1:2]

#Imprimimos los numeros
print(numero1)
print(numero2)


