'''Elabore un programa en Python que lea una longitud en metros y la convierta y escriba en centímetros, pies y pulgadas.'''

#Variable donde se almacena la langitud en metros
metros = int(input('Porfavor ingrese la longitud en metros: -->'))

#Calculamos las medidas solicitadas
centimetros = 100 * metros
pies = 3.28084 * metros
pulgadas = 39.37008 * metros

print(f'''Luego de realizar la conversión de {metros} metros se tiene su equivalencia en las siguientes medidas:
* centimetros: {centimetros}
* pies: {pies}
* pulgadas: {pulgadas}''')
