from clases.clase_vector import vector

n = int(input('Ingrese el tamaño del vector a crear: --> '))

#Creamos al vector
vec1 = vector(n)
vec1.construyeVector(n//2)

m = int(input('Ingrese el tamaño del nuevo vector a crear: --> '))

#Creamos al vector
vec2 = vector(n)
vec2.construyeVector(n//2,200)

vec1.seleccion()
vec2.seleccion()

vec3 = vector(m+n)

i=1
j=1

#Los siguientes while intercalan los datos de los dos anteriores
while i <= vec1.V[0] and j <= vec2.V[0]:
    if vec1.V[i] < vec2.V[j]:
        vec3.agregarDato(vec1.V[i])
        i += 1
    else:
        vec3.agregarDato(vec2.V[j])
        j += 1

while i <= vec1.V[0]:
    vec3.agregarDato(vec1.V[i])
    i += 1

while j <= vec2.V[0]:
    vec3.agregarDato(vec2.V[j])
    j += 1


vec1.imprimeVector('Vector prueba 1:')
vec2.imprimeVector('Vector prueba 2:')
vec3.imprimeVector('Vector Intercalado:')
