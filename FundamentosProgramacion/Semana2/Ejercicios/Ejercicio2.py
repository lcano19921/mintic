'''Elabore programa en Python que le permita al usuario ingresar números enteros de manera indefinida, 
hasta que ingrese un número negativo, y al final imprimir la suma de los números enteros pares sin incluir el 
número negativo en la suma.'''

sw = 1
#La variable sw es una bandera o suiche, se utiliza para que el ciclo se repita
#hasta que se cumpla una condicion, en este caso un numero negativo
sum_pares = 0
#Este es un acumulador, donde se van a sumar los numeros pares
while sw == 1:
    #Le indico al ciclo que siempre se ejecute si la bandera o suiche está encendida (previamente lo hicimos)
    numero = int(input('Porfavor ingrese un numero entero: '))
    #Solicito el numero y lo declaro como entero
    if numero >= 0:
        #Valido que no sea un numero negativo
        if numero%2 == 0:
            #Valido si es par
            sum_pares += numero
            #Lo sumo a los numeros anteriores, esta instruccion es similar a: sum_pares = sum_pares + numero
    else:
        #Ingresa a este else cuando el numero es negativo
        sw = 0
        #Apagamos la bandera para que se SALGA del ciclo

print(f'El total de la suma de los numeros pares es: {sum_pares}')
#Imprimimos el mensaje