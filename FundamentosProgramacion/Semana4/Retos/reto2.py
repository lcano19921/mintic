from math import sqrt
import numpy as np

def createMatriz(n):
    matriz = []    
    for i in range(n):
        aux = [0]*n
        matriz.append(aux)
    return matriz	

def matrizDimensionFromList(lista):
    n = 0
    aux = 0    
    sw = 0
    while sw <= len(lista):      
        aux2 = ((aux*2)+1)
        aux = aux2        
        sw = aux2     
        n += 1            
    return n         

def fillMatriz(matriz,lista:list,n):     
    for i in range(0, n):
        for j in range(0,i+1):            
            matriz[i][j] = lista[0]            
            lista.pop(0)       
        
    return matriz

def solucion(lista):

    n = matrizDimensionFromList(lista)
    matriz = createMatriz(n) 

    matriz = fillMatriz(matriz,lista,n)

    return matriz

    
"""
Estas líneas de código que siguen, permiten probar si su ejercicio es correcto
"""
#NO MODIFICAR
l = [8, 60, 72, 35, 26, 75, 15, 12, 33, 54]
matriz_correcta = np.array([[ 8.,  0.,  0.,  0.], [60., 72.,  0.,  0.], [35., 26., 75.,  0.], [15., 12., 33., 54.]])
matriz_estudiante = np.array(solucion(l))
print("LISTA ENTREGADA:\n", l,"\n")
print("===SALIDA ESPERADA===\nMatriz:\n", matriz_correcta,"\n")
print("===TU SALIDA===\nMatriz:\n", matriz_estudiante,"\n")

for i in range(matriz_correcta.shape[0]):
    for j in range(matriz_correcta.shape[1]):
        if matriz_correcta[i,j] != matriz_estudiante[i,j]:
            print("Las salidas no coinciden, ¡Estás olvidando algo en tu código!")
            exit()
print("Todo se ve correcto, ¡Procede a calificar tu código!")


